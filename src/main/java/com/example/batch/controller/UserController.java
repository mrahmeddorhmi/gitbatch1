package com.example.batch.controller;

import com.example.batch.batchUser.ConfigBatchUser;
import com.example.batch.dbToCsv.ConfigDbToCsv;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/loadusers")
public class UserController {
    @Autowired private JobLauncher jobLauncher;
    @Autowired private ConfigBatchUser job;
    @Autowired private ConfigDbToCsv JobConfigDbToCsv;

    @GetMapping("/loadusertodb")
    public BatchStatus loadUsers() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        Map<String, JobParameter> params = new HashMap<>();

        params.put("time", new JobParameter(System.currentTimeMillis()));

        JobParameters jobParameters = new JobParameters(params);
        JobExecution jobExecution = jobLauncher.run(job.job(), jobParameters);

        while (jobExecution.isRunning()) {
            System.out.println("...");
        }

        return jobExecution.getStatus();
    }

    @GetMapping("/loadusertocsv")
    public BatchStatus loadUsersToCsv() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        Map<String, JobParameter> params = new HashMap<>();

        params.put("time", new JobParameter(System.currentTimeMillis()));

        JobParameters jobParameters = new JobParameters(params);
        JobExecution jobExecution = jobLauncher.run(JobConfigDbToCsv.jobToScv(), jobParameters);

        while (jobExecution.isRunning()) {
            System.out.println("...");
        }
        return jobExecution.getStatus();

    }
}
