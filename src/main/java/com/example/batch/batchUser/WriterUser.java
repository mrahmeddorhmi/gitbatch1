package com.example.batch.batchUser;

import com.example.batch.model.User;
import com.example.batch.repository.UserRepository;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Qualifier("toDB")
public class WriterUser implements ItemWriter<User> {
    @Autowired private UserRepository userRepository;
    @Override
    public void write(List<? extends User> list) throws Exception {
        userRepository.saveAll(list);
    }
}
