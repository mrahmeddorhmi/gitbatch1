package com.example.batch.batchUser;

import com.example.batch.model.User;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileNotFoundException;

@Configuration
@EnableBatchProcessing
@Qualifier("toDB")
public class ConfigBatchUser {
    @Autowired private JobBuilderFactory jobBuilderFactory;
    @Autowired private StepBuilderFactory stepBuilderFactory;
    @Qualifier("toDB")
    @Autowired private ItemReader<User> itemReader;
    @Qualifier("toDB")
    @Autowired private ItemProcessor<User, User> userUserItemProcessor;
    @Qualifier("toDB")
    @Autowired private ItemWriter<User> userItemWriter;


    @Bean
    @Qualifier("toDB")
    public Job job() {
        Step step = stepBuilderFactory.get("user_step_1")
                .<User, User>chunk(100)
                .reader(itemReader)
                .processor(userUserItemProcessor)
                .writer(userItemWriter)
                .build();
        Job job = jobBuilderFactory.get("user_job_1")
                .start(step)
                .build();
        return job;
    }

    @Bean
    @Qualifier("toDB")
    public FlatFileItemReader<User> userFlatFileItemReader()  {

        FlatFileItemReader<User> flatFileItemReader = new FlatFileItemReader<>();

        flatFileItemReader.setName("csv-reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setResource(new ClassPathResource("file.csv"));
        flatFileItemReader.setLineMapper(userLineMapper());

        return flatFileItemReader;


    }

    /**
     *
     * @return
     */
    @Bean
    @Qualifier("toDB")
    public LineMapper<User> userLineMapper() {
        DefaultLineMapper<User> userDefaultLineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer userDelimitedLineTokenizer = new DelimitedLineTokenizer();

        userDelimitedLineTokenizer.setDelimiter(",");
        userDelimitedLineTokenizer.setStrict(false);
        userDelimitedLineTokenizer.setNames("id", "name", "number", "department");

        userDefaultLineMapper.setLineTokenizer(userDelimitedLineTokenizer);
        BeanWrapperFieldSetMapper<User> userBeanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();

        userBeanWrapperFieldSetMapper.setTargetType(User.class);

        userDefaultLineMapper.setFieldSetMapper(userBeanWrapperFieldSetMapper);

        return userDefaultLineMapper;


    }

}
