package com.example.batch.dbToCsv;

import com.example.batch.model.User;
import com.example.batch.repository.UserRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableBatchProcessing
@Qualifier("toCSV")
public class ConfigDbToCsv {
    @Autowired private JobBuilderFactory jobBuilderFactory;
    @Autowired private StepBuilderFactory stepBuilderFactory;
    @Autowired private DataSource dataSource;
    @Autowired private UserRepository userRepository;

    @Qualifier("toCSV")
    @Autowired private RepositoryItemReader<User> itemReader;
    @Qualifier("toCSV")
    @Autowired private ItemProcessor<User, User> userUserItemProcessor;
    @Qualifier("toCSV")
    @Autowired private ItemWriter<User> userItemWriter;

    @Bean
    public Job jobToScv() {
        Step step = stepBuilderFactory.get("user_step_1_to_csv")
                .<User, User>chunk(100)
                .reader(itemReader)
                .processor(userUserItemProcessor)
                .writer(userItemWriter)
                .build();
        Job job = jobBuilderFactory.get("user_job_1_to_csv")
                .start(step)
                .build();
        return job;
    }

    @Bean
    @Qualifier("toCSV")
    public RepositoryItemReader<User> reader() {
        RepositoryItemReader<User> reader = new RepositoryItemReader<>();
        reader.setRepository(userRepository);
        reader.setMethodName("findAll");

        Map<String, Sort.Direction> sort = new HashMap<String, Sort.Direction>();
        sort.put("id", Sort.Direction.ASC);
        reader.setSort(sort);

        return reader;
    }

}
