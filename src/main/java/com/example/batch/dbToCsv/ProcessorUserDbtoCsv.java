package com.example.batch.dbToCsv;

import com.example.batch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("toCSV")
public class ProcessorUserDbtoCsv implements ItemProcessor<User, User> {

@Override
public User process(User user) throws Exception {
        return user;
    }
}