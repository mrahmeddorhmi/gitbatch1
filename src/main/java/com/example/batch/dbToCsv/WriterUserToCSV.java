package com.example.batch.dbToCsv;

import com.example.batch.model.User;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

import java.util.List;
@Qualifier("toCSV")
@Component
public class WriterUserToCSV implements ItemWriter<User> {
    @Override
    public void write(List<? extends User> list) throws Exception {
        FlatFileItemWriter<User> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource("src/main/resources/dbtofile.csv"));
        writer.open(new ExecutionContext());

        System.out.println("ici");
        DelimitedLineAggregator<User> userDelimitedLineAggregator = new DelimitedLineAggregator<>();
        userDelimitedLineAggregator.setDelimiter(",");

        BeanWrapperFieldExtractor<User> fieldExtractor = new BeanWrapperFieldExtractor<>();
        fieldExtractor.setNames(new String[]{"id", "name", "number", "department"});
        userDelimitedLineAggregator.setFieldExtractor(fieldExtractor);

        writer.setLineAggregator(userDelimitedLineAggregator);
        writer.setAppendAllowed(true);
        writer.setShouldDeleteIfEmpty(true);
        writer.setShouldDeleteIfExists(true);
        writer.write(list);
        writer.close();

    }
}
